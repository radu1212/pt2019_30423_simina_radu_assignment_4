package BusinessLayer;

import DataLayer.ResourceManager;
import DataLayer.RestaurantSerializator;
import com.sun.org.apache.xpath.internal.operations.Or;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

public class Restaurant extends Observable {

    private ArrayList<MenuItem> menu;
    private HashMap<Order,ArrayList<MenuItem>> orders;

    public Restaurant() {
        this.menu = new ArrayList<>();
        this.orders = new HashMap<Order,ArrayList<MenuItem>>(){{
            put(new Order(1,"2019-05-16",1),new ArrayList<>());
        }};

        RestaurantSerializator data = null;
        try {
            data = ResourceManager.load("restaurantDetails.txt");
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (data != null) {
            this.menu = data.menu;
        }

    }

    public void notified(){
        setChanged();
    }

    public void createNewMenuItem(MenuItem menuItem){
        menu.add(menuItem);
    }

    public void removeMenuItem(MenuItem menuItem){
        menu.remove(menuItem);
    }

    public void editMenuItem(int id, String newProvider, int newPrice){
        for(MenuItem m: menu){
            if(m.getId() == id){
                m.setProvider(newProvider);
                m.setPrice(newPrice);
            }
        }
    }

    public ArrayList<MenuItem> getMenu() {
        return menu;
    }

    public void setMenu(ArrayList<MenuItem> menu) {
        this.menu = menu;
    }

    public HashMap<Order, ArrayList<MenuItem>> getOrders() {
        return orders;
    }

    public void setOrders(HashMap<Order, ArrayList<MenuItem>> orders) {
        this.orders = orders;
    }

    public boolean verifyOrderDetails(Order order) {
        if(order.getOrderId() < 0) return false;
        if(!new FormattedDateMatcher().matches(order.getDate())) return false;
        return !(order.getTable() < 0 || order.getTable() > 15);
    }


    public void createNewOrder(int orderNo, String date, int table){
        orders.put(new Order(orderNo, date, table), new ArrayList<MenuItem>());
    }


    public void printBill(Order order){
        int total = 0;
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("bill.txt"), "utf-8"))) {
            writer.write(order.toString() +"\n");
            for (MenuItem item : orders.get(order)) {

                total += item.getPrice();
                writer.write(item.toString() );
            }
            writer.write("\nTotal amount to be paid:  " + total + " RON ");
        } catch (IOException ex) {
            // Report
        }
    }
}
