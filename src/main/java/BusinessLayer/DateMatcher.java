package BusinessLayer;

public interface DateMatcher {
    boolean matches(String date);
}
