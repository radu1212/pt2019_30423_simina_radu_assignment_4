package BusinessLayer;

import java.io.Serializable;
import java.util.ArrayList;

public class CompositeProduct extends MenuItem implements Serializable {

    private ArrayList<MenuItem> components;

    public CompositeProduct(int id, String provider, int price) {
        super(id, provider, price);
        this.components = new ArrayList<>();
    }

    @Override
    public int computePrice() {
        int finalPrice = 0;
        for(MenuItem menuItem:components){
            finalPrice += menuItem.getPrice();
        }
        return finalPrice;
    }

    public ArrayList<MenuItem> getComponents() {
        return components;
    }

    public void setComponents(ArrayList<MenuItem> components) {
        this.components = components;
    }
}
