package BusinessLayer;

import java.io.Serializable;

public class BaseProduct extends MenuItem implements Serializable {


    public BaseProduct(int id, String provider, int price) {
        super(id, provider, price);
    }


    @Override
    int computePrice() {
        return getPrice();
    }
}
