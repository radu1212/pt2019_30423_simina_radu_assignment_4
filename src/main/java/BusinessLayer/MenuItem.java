package BusinessLayer;

import java.io.Serializable;

public abstract class MenuItem implements Serializable {
    private int id;
    private String provider;
    private int price;

    public MenuItem(int id, String provider, int price) {
        this.id = id;
        this.provider = provider;
        this.price = price;
    }

    public MenuItem() { }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public boolean verifyProduct(){
        if(id<0) return false;
        if(provider.equals("")) return false;
        return price >= 0 && price <= 1000;
    }

    abstract int computePrice();

    @Override
    public String toString() {
        return "Product:    " +
                "id = " + id +
                ",    name = " + provider +
                ",    price = " + price + "\n";
    }
}
