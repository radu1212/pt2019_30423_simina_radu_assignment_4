package DataLayer;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ResourceManager {
    public static void save(Serializable data,String fileName) throws Exception{
        FileOutputStream fileOutput = new FileOutputStream(fileName);
        ObjectOutputStream outputStream = new ObjectOutputStream(fileOutput);
        outputStream.writeObject(data);
        fileOutput.close();
        outputStream.close();
    }
    public static RestaurantSerializator load(String fileName) throws Exception{
        FileInputStream fiStream = new FileInputStream(fileName);
        ObjectInputStream objectStream = new ObjectInputStream(fiStream);
        RestaurantSerializator object = (RestaurantSerializator)objectStream.readObject();
        fiStream.close();
        objectStream.close();
        return object;
    }
}
