package DataLayer;

import BusinessLayer.MenuItem;
import BusinessLayer.Order;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RestaurantSerializator implements Serializable {
    private static final long serialVersionUID = 1L;

    public ArrayList<MenuItem> menu;
    public transient HashMap<Order,ArrayList<MenuItem>> orders;

    public RestaurantSerializator(ArrayList<MenuItem> menu, HashMap<Order, ArrayList<MenuItem>> orders) {
        this.menu = menu;
        this.orders = orders;
    }
}
