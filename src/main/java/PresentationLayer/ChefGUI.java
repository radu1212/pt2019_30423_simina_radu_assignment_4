package PresentationLayer;

import javax.swing.*;
import java.util.Observable;
import java.util.Observer;

public class ChefGUI implements Observer {

    @Override
    public void update(Observable o, Object arg) {
        JOptionPane.showMessageDialog(null, "Chef was notified!", "Chef notification", JOptionPane.INFORMATION_MESSAGE);
    }
}
