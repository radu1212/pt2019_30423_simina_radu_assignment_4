package PresentationLayer;


import BusinessLayer.MenuItem;
import BusinessLayer.Order;
import BusinessLayer.Restaurant;
import DataLayer.ResourceManager;
import DataLayer.RestaurantSerializator;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import static org.junit.Assert.*;
import java.util.Calendar;


    public class WaiterGUI extends JFrame {
        Restaurant restaurant;
        private int orderNo = 1;
        private JTextField tableNumber;
        private JTextField orderId;
        private JTextField it;
        private JButton addNewItem;
        private JButton addNewOrder;
        private JButton viewAllOrders;
        private JButton printBill;
        private JTable orders;


        public WaiterGUI(String s) throws HeadlessException {

            restaurant = new Restaurant();
            ChefGUI chef = new ChefGUI();
            restaurant.addObserver(chef);
            new AdminGUI(restaurant,"Admin");
            try {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            setTitle(s);
            setSize(800,600);

            Font f = new Font("Yatra One", Font.PLAIN, 12);
            Font f2 = new Font("Yatra One", Font.PLAIN, 12);
            Font f1 = new Font("Karla", Font.PLAIN, 30);

            JPanel panel = new JPanel();
            panel.setVisible(true);
            panel.setEnabled(true);
            panel.setLayout(null);

            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            setContentPane(panel);

            JLabel waiterInterface = new JLabel("Graphical interface for the waiter");
            waiterInterface.setFont(f1);
            waiterInterface.setHorizontalAlignment(JLabel.CENTER);
            waiterInterface.setBounds(30,20,600,50);
            waiterInterface.setVisible(true);
            panel.add(waiterInterface);
            
            JLabel newOrder = new JLabel("Please insert the number of the table for which you wish to create a new order: ");
            newOrder.setFont(f);
            newOrder.setHorizontalAlignment(JLabel.LEFT);
            newOrder.setBounds(10,70,450,50);
            newOrder.setVisible(true);
            panel.add(newOrder);

            tableNumber = new JTextField();
            tableNumber.setFont(f);
            tableNumber.setHorizontalAlignment(SwingConstants.CENTER);
            tableNumber.setBounds(100,130,100,30);
            tableNumber.setVisible(true);
            panel.add(tableNumber);

            JLabel order = new JLabel("Please insert the id of the order that you want to print the bill for:  ");
            order.setFont(f);
            order.setHorizontalAlignment(JLabel.LEFT);
            order.setBounds(10,170,450,50);
            order.setVisible(true);
            panel.add(order);

            JLabel items = new JLabel("Please select the order and insert the id of the product:  ");
            items.setFont(f);
            items.setHorizontalAlignment(JLabel.LEFT);
            items.setBounds(420,120,450,50);
            items.setVisible(true);
            panel.add(items);

            it = new JTextField();
            it.setFont(f);
            it.setHorizontalAlignment(SwingConstants.CENTER);
            it.setBounds(520,160,100,30);
            it.setVisible(true);
            panel.add(it);

            addNewItem = new JButton("Add new item to an order");
            addNewItem.setFont(f2);
            addNewItem.setHorizontalAlignment(SwingConstants.CENTER);
            addNewItem.setBounds(475,210,180,30);
            addNewItem.setVisible(true);
            panel.add(addNewItem);

            orderId = new JTextField();
            orderId.setFont(f);
            orderId.setHorizontalAlignment(SwingConstants.CENTER);
            orderId.setBounds(100,220,100,30);
            orderId.setVisible(true);
            panel.add(orderId);

            addNewOrder = new JButton("Add new order");
            addNewOrder.setFont(f2);
            addNewOrder.setHorizontalAlignment(SwingConstants.CENTER);
            addNewOrder.setBounds(50,320,150,30);
            addNewOrder.setVisible(true);
            panel.add(addNewOrder);

            viewAllOrders = new JButton("View all orders");
            viewAllOrders.setFont(f2);
            viewAllOrders.setHorizontalAlignment(SwingConstants.CENTER);
            viewAllOrders.setBounds(50,370,150,30);
            viewAllOrders.setVisible(true);
            panel.add(viewAllOrders);

            printBill = new JButton("Print a bill");
            printBill.setFont(f2);
            printBill.setHorizontalAlignment(SwingConstants.CENTER);
            printBill.setBounds(50,420,150,30);
            printBill.setVisible(true);
            panel.add(printBill);

            orders = new JTable();
            Object[] columns = {"OrderId","Date","Table"};
            DefaultTableModel model = new DefaultTableModel();
            model.setColumnIdentifiers(columns);
            orders.setModel(model);
            orders.setBackground(Color.LIGHT_GRAY);
            orders.setForeground(Color.black);
            Font font = new Font("",1,16);
            orders.setFont(font);
            orders.setRowHeight(20);
            JScrollPane pane = new JScrollPane(orders);
            pane.setBounds(300, 280, 450, 250);



            final Object[] row = new Object[3];

            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate = LocalDate.now();

            // button add row
            addNewOrder.addActionListener(e -> {
                String date = dtf.format(localDate);
                row[0] = orderNo;
                row[1] = dtf.format(localDate);
                row[2] = tableNumber.getText();

                model.addRow(row);

                Order order1 = new Order(orderNo,date,Integer.parseInt(tableNumber.getText()));

                restaurant.getOrders().put(order1,new ArrayList<>());

                orderNo++;

                try {
                    ResourceManager.save(new RestaurantSerializator(restaurant.getMenu(),restaurant.getOrders()),"restaurantDetails.txt");
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            });

            addNewItem.addActionListener(e -> {
                restaurant.notified();
                restaurant.notifyObservers();
                addMenuItemToOrder();
                try {
                    ResourceManager.save(new RestaurantSerializator(restaurant.getMenu(),restaurant.getOrders()),"restaurantDetails.txt");
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            });

            viewAllOrders.addActionListener(e -> {
                panel.add(pane);
                try {
                    ResourceManager.save(new RestaurantSerializator(restaurant.getMenu(),restaurant.getOrders()),"restaurantDetails.txt");
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            });

            printBill.addActionListener(e -> {
                printTheBill();
                try {
                    ResourceManager.save(new RestaurantSerializator(restaurant.getMenu(),restaurant.getOrders()),"restaurantDetails.txt");
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            });

            setVisible(true);
        }


        public static void main(String[] args) {
            new WaiterGUI("Waiter");
        }
    }


