package PresentationLayer;

import BusinessLayer.BaseProduct;
import BusinessLayer.CompositeProduct;
import BusinessLayer.MenuItem;
import BusinessLayer.Restaurant;
import DataLayer.ResourceManager;
import DataLayer.RestaurantSerializator;

import static org.junit.Assert.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class AdminGUI extends JFrame {


    private CompositeProduct product;
    private JTable menu;
    private JTextField productId;
    private JTextField provider;
    private JTextField price;
    private JTextField newItem;
    private JButton add;
    private JButton edit;
    private JButton delete;
    private JButton create;
    private JButton addToComposite;
    private JButton done;

    public AdminGUI(Restaurant restaurant, String s) throws HeadlessException {

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        setTitle(s);
        setSize(500, 550);

        Font f = new Font("Yatra One", Font.PLAIN, 12);
        Font f2 = new Font("Yatra One", Font.PLAIN, 12);
        Font f1 = new Font("Karla", Font.PLAIN, 30);

        JPanel panel = new JPanel();
        panel.setVisible(true);
        panel.setEnabled(true);
        panel.setLayout(null);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setContentPane(panel);

        menu = new JTable();
        Object[] columns = {"Id","Name","Price"};
        DefaultTableModel model = new DefaultTableModel();
        model.setColumnIdentifiers(columns);
        menu.setModel(model);
        menu.setBackground(Color.LIGHT_GRAY);
        menu.setForeground(Color.black);
        Font font = new Font("",1,16);
        menu.setFont(font);
        menu.setRowHeight(20);
        JScrollPane pane = new JScrollPane(menu);
        pane.setBounds(0, 0, 500, 280);
        panel.add(pane);

        add = new JButton("Add new product");
        add.setFont(f2);
        add.setHorizontalAlignment(SwingConstants.CENTER);
        add.setBounds(10,370,140,30);
        add.setVisible(true);
        panel.add(add);

        edit = new JButton("Edit existing product");
        edit.setFont(f2);
        edit.setHorizontalAlignment(SwingConstants.CENTER);
        edit.setBounds(170,370,160,30);
        edit.setVisible(true);
        panel.add(edit);

        delete = new JButton("Delete product");
        delete.setFont(f2);
        delete.setHorizontalAlignment(SwingConstants.CENTER);
        delete.setBounds(350,370,120,30);
        delete.setVisible(true);
        panel.add(delete);

        productId = new JTextField();
        productId.setFont(f2);
        productId.setHorizontalAlignment(SwingConstants.CENTER);
        productId.setBounds(10,330,140,30);
        productId.setVisible(true);
        panel.add(productId);

        provider = new JTextField();
        provider.setFont(f2);
        provider.setHorizontalAlignment(SwingConstants.CENTER);
        provider.setBounds(170,330,160,30);
        provider.setVisible(true);
        panel.add(provider);

        price = new JTextField();
        price.setFont(f2);
        price.setHorizontalAlignment(SwingConstants.CENTER);
        price.setBounds(350,330,120,30);
        price.setVisible(true);
        panel.add(price);

        JLabel Id = new JLabel("ID");
        Id.setFont(f2);
        Id.setHorizontalAlignment(SwingConstants.CENTER);
        Id.setBounds(10,300,140,30);
        Id.setVisible(true);
        panel.add(Id);

        JLabel pro = new JLabel("Name");
        pro.setFont(f2);
        pro.setHorizontalAlignment(SwingConstants.CENTER);
        pro.setBounds(170,300,160,30);
        pro.setVisible(true);
        panel.add(pro);

        JLabel pri = new JLabel("Price(RON)");
        pri.setFont(f2);
        pri.setHorizontalAlignment(SwingConstants.CENTER);
        pri.setBounds(350,300,120,30);
        pri.setVisible(true);
        panel.add(pri);

        create = new JButton("Create");
        create.setFont(f2);
        create.setHorizontalAlignment(SwingConstants.CENTER);
        create.setBounds(10,470,140,30);
        create.setVisible(true);
        panel.add(create);

        addToComposite = new JButton("Add item");
        addToComposite.setFont(f2);
        addToComposite.setHorizontalAlignment(SwingConstants.CENTER);
        addToComposite.setBounds(170,470,160,30);
        addToComposite.setVisible(true);
        panel.add(addToComposite);

        newItem = new JTextField("Insert the id of the new product you want to add to the the menu");
        newItem.setFont(f2);
        newItem.setHorizontalAlignment(SwingConstants.CENTER);
        newItem.setBounds(10,420,450,30);
        newItem.setVisible(true);
        panel.add(newItem);

        final Object[] row = new Object[3];

        for(MenuItem m:restaurant.getMenu()){
            row[0] = m.getId();
            row[1] = m.getProvider();
            row[2] = m.getPrice();
            model.addRow(row);
        }

        add.addActionListener(e -> {

            row[0] = Integer.parseInt(productId.getText());
            row[1] = provider.getText();
            row[2] = Integer.parseInt(price.getText());

            model.addRow(row);
            restaurant.getMenu().add(new BaseProduct(Integer.parseInt(productId.getText()),provider.getText(),Integer.parseInt(price.getText())));
            try {
                ResourceManager.save(new RestaurantSerializator(restaurant.getMenu(),restaurant.getOrders()),"restaurantDetails.txt");
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        });

        edit.addActionListener((e -> {

            int i = menu.getSelectedRow();
            if(i>0) {
                menu.setValueAt(productId.getText(), i, 0);
                menu.setValueAt(provider.getText(), i, 1);
                menu.setValueAt(price.getText(), i, 2);
            }
            int prodId = Integer.parseInt(productId.getText());
            MenuItem menuItem = null;

            for(MenuItem m:restaurant.getMenu()){
                if(m.getId() == prodId){
                    menuItem = m;
                }
            }

            assertTrue(menuItem.verifyProduct());
            menuItem.setPrice(Integer.parseInt(price.getText()));
            menuItem.setProvider(provider.getText());

            try {
                ResourceManager.save(new RestaurantSerializator(restaurant.getMenu(),restaurant.getOrders()),"restaurantDetails.txt");
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }));

        delete.addActionListener(e -> {
            int i = menu.getSelectedRow();


            int prodId = Integer.parseInt(menu.getValueAt(i,0).toString());

            MenuItem menuItem = null;

            for(MenuItem m:restaurant.getMenu()){
                if(m.getId() == prodId){
                    menuItem = m;
                }
            }
            assertTrue(menuItem.verifyProduct());
            restaurant.getMenu().remove(menuItem);

            try {
                ResourceManager.save(new RestaurantSerializator(restaurant.getMenu(),restaurant.getOrders()),"restaurantDetails.txt");
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            model.removeRow(i);
        });

        create.addActionListener(e -> {
            row[0] = newItem.getText();
            row[1] = "Composite product";
            row[2] = 0;

            model.addRow(row);

            MenuItem menuItem = new CompositeProduct(Integer.parseInt(newItem.getText()),"Composite product", 0);
            restaurant.getMenu().add(menuItem);
            product = new CompositeProduct(Integer.parseInt(newItem.getText()),"Composite product",0);
        });

        addToComposite.addActionListener(e -> {
            int i = menu.getSelectedRow();
            MenuItem menuItem = null;
            if(i>0) {
                int idOfSelectedProduct = Integer.parseInt(menu.getValueAt(i, 0).toString());

                for (MenuItem m : restaurant.getMenu()) {
                    if (m.getId() == idOfSelectedProduct) {
                        menuItem = m;
                    }
                }
            }

            assert menuItem != null;
            assertTrue(menuItem.verifyProduct());
            product.getComponents().add(menuItem);

            for(int j = 0; j < restaurant.getMenu().size(); j++){
                if(Integer.parseInt(newItem.getText()) ==  Integer.parseInt(menu.getValueAt(j,0).toString())){
                    menu.setValueAt(product.computePrice(),j,2);
                }
            }

            try {
                ResourceManager.save(new RestaurantSerializator(restaurant.getMenu(),restaurant.getOrders()),"restaurantDetails.txt");
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        });

        setVisible(true);
    }


    public static void main(String[] args) {
        new AdminGUI(new Restaurant(),"test");
    }
}
